# Agenda

We’ll follow this tentative agenda on the day of the training (actual times may vary):

## 00:00 to 00:50 (first hour)
* Introductions and icebreakers
* Questions from the pre-work:
  * Git for the True Beginner
  * Introduction to the command-line interface
  * Markdown tutorial
* Review of Git concepts and learn about Microsoft Visual Studio Code (VS Code)
* Live demo of working with Git in VS Code

## 00:50 to 01:00 - Ten minute break

## 01:00 to 01:50 (second hour)

* We’ll help you:
  * Set up your SSH keys to contribute
  * Get added to Git Training project repository
  * Fork and clone the project repository

## 01:50 to 02:00 - Ten minute break

## 02:00 to 02:50 (third hour)
* Everyone will be assigned a website issue to fix with the goal of submitting their first merge request
* We’ll stand by and help you while you work on Git on your own for the first time
* You’ll leave the meeting with a homework item to create a Git knowledge base
* If time:
  * Closing tips and tricks about using Git
  * Recommended extensions for VS Code
  * Feedback on the training session
