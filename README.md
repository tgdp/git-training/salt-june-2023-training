# The Good Dogs Project

A Hugo-based site using Bryan's theme for the Good Docs Project's Git training workshop.

Hosted on Netlify: [https://salt-june-2023-training.netlify.app/](https://salt-june-2023-training.netlify.app/)

To view the training agenda: [https://salt-june-2023-training.netlify.app/agenda/](https://salt-june-2023-training.netlify.app/agenda/)
